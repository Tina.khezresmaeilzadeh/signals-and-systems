%% QUESTION 1

%PART 1
aa=imread('house.jpg');
aa=rgb2gray(aa);
Gx=[-1 0 1;-2 0 2;-1 0 1];
Gy=[-1 -2 -1;0 0 0;1 2 1];
aa1=conv2(Gx,aa);
aa2=conv2(Gy,aa);
aa3=sqrt(aa1.^2+aa2.^2);
aa3=uint8(aa3);%converting to uint8
figure
imshow(aa3);
size(aa3)

%% 
xcenter=imread('house.jpg');
ycenter=rgb2gray(xcenter);
g1=[5 5 5;-3 0 -3;-3 -3 -3];
g2=[5 5 -3;5 0 -3;-3 -3 -3];
g3=[5 -3 -3;5 0 -3;5 -3 -3];
g4=[-3 -3 -3;5 0 -3;5 5 -3];
g5=[-3 -3 -3;-3 0 -3;5 5 5];
g6=[-3 -3 -3;-3 0 5;-3 5 5];
g7=[-3 -3 5;-3 0 5;-3 -3 5];
g8=[-3 5 5;-3 0 5;-3 -3 -3];
%
NormMatrix=zeros(8,1);
aa1=conv2(g1,ycenter);
bb1=conv2(g1,ycenter);
cc1=sqrt(aa1.^2+bb1.^2);
cc1=uint8(cc1);
NormMatrix(1)=norm(dd1,2);
aa2=conv2(g2,ycenter);
bb2=conv2(g2,ycenter);
cc2=sqrt(aa2.^2+bb2.^2);
NormMatrix(2)=norm(dd2,2);
cc2=uint8(cc2);
aa3=conv2(g1,ycenter);
bb3=conv2(g1,ycenter);
cc3=sqrt(aa3.^2+bb3.^2);
cc3=uint8(cc3);
NormMatrix(3)=norm(dd3,2);
aa4=conv2(g1,ycenter);
bb4=conv2(g1,ycenter);
cc4=sqrt(aa4.^2+bb4.^2);
NormMatrix(4)=norm(dd4,2);
cc4=uint8(cc4);
aa5=conv2(g1,ycenter);
bb5=conv2(g1,ycenter);
cc5=sqrt(aa5.^2+bb5.^2);
cc5=uint8(cc5);
NormMatrix(5)=norm(dd5,2);
aa6=conv2(g1,ycenter);
bb6=conv2(g1,ycenter);
cc6=sqrt(aa6.^2+bb6.^2);
cc6=uint8(cc6);
NormMatrix(6)=norm(dd6,2);
aa7=conv2(g1,ycenter);
bb7=conv2(g1,ycenter);
cc7=sqrt(aa7.^2+bb7.^2);
cc7=uint8(cc7);
NormMatrix(7)=norm(dd7,2);
aa8=conv2(g1,ycenter);
bb8=conv2(g1,ycenter);
cc8=sqrt(aa8.^2+bb8.^2);
cc8=uint8(cc8);
NormMatrix(8)=norm(dd8,2);
o1=max(cc1,cc2);
o2=max(o1,cc3);
o3=max(o2,cc4);
o4=max(o3,cc5);
o5=max(o4,cc6);
o6=max(o5,cc7);
o7=max(o6,cc8);
imshow(o7);


%% QUESTION 2
 %PROBLEM 2
 clear
 clc
aa=imread('circles.jpg');
aa=rgb2gray(aa);
Gx=[-1 0 1;-2 0 2;-1 0 1];
Gy=[-1 -2 -1;0 0 0;1 2 1];
aa1=conv2(Gx,aa);
aa2=conv2(Gy,aa);
aa3=sqrt(aa1.^2+aa2.^2);
image2= uint8(aa3);%converting to uint8
[row,col]=size(image2);
Accumulator=zeros(row,col,140);
for i=1:row
    for j=1:col
        if image2(i,j)==1
            for r=12:0.1:14
                for teta=0:360
                    xcenter = round(i+r*cos(teta*pi/180));
                    ycenter = round(j+r*sin(teta*pi/180));
                    if xcenter>0 && ycenter>0 && xcenter<row && ycenter<col
                    Accumulator(xcenter,ycenter,10*r)=Accumulator(xcenter,ycenter,10*r)+1;
                    end 
                end
            end
        end
    end
end
numberofcircles=0;
for r=12:0.1:14
    region=max(max(Accumulator(:,:,10*r)));
    for i=1:row
        for j=1:col
            if  abs(Accumulator(i,j,10*r)-region)<1
                numberofcircles=numberofcircles+1;
            end
        end
    end
end
disp(numberofcircles)
% clear
% clc
% aa=imread('circles.jpg');
% aa=rgb2gray(aa);
% Gx=[-1 0 1;-2 0 2;-1 0 1];
% Gy=[-1 -2 -1;0 0 0;1 2 1];
% aa1=conv2(Gx,aa);
% aa2=conv2(Gy,aa);
% aa3=sqrt(aa1.^2+aa2.^2);
% c = uint8(aa3);%converting to uint8
% [rw, col] = size(c);
% A=zeros(rw,col);
% for i=1:rw
%     disp(i);
%     for j=1:col
%         if(c(i,j)==1)
%             for r=20:41
%                 for t=0:360
%                     a=floor(i+r*cos(pi*t/180));
%                     b=floor(j+r*sin(pi*t/180));
%                     cq=a+1;
%                     d=b+1;
%                     if a>0 && b>0 && cq<rw && d<col
%                         A(a,b)=A(a,b)+1;
%                         A(cq,b)=A(cq,b)+1;
%                         A(a,d)=A(a,d)+1;
%                         A(cq,d)=A(cq,d)+1;
% 
%                     end
%                 end
%             end
%         end
%     end
% end
% sum=zeros(rw,col);
% for i=1:rw
%     for j=1:col
%         if A(i,j)>0.5*max(max(A))
%             sum(i,j)=1;
%         end
%     end
% end
% count=0;
% check=0;
% indexx=[];
% indexy=[];
% for i=1:rw
%     for j=1:col
%         for k=1:size(indexx)
%           if abs(i-indexx(k))<6 && abs(j-indexy(k))<6
%               check=1;
%              
%           end
%         indexx=[indexx i];
%         indexy=[indexy j];
%         if sum(i,j)==1 && check==0
%             count=count+1;
%         end
%         end
%     end
% end
% count;
        
        


 
%% QUESTION 3
clear
  clc
  A=imread('pic1.png');
  B=imread('pic2.png');
  fft2A=fft2(A);
  fft2B=fft2(B);
  figure
  imshow(fft2A);
  figure
  imshow(fft2B);
  ph_fft2C=angle(fft2A);
  abs_fft2C=abs(fft2B);
  C=abs_fft2C.*exp(1i*(ph_fft2C));
  ifftc=ifft2(C);
  ifftc=uint8(ifftc);
  figure
  imshow(ifftc);
  
  %% QUESTION 4
  xcenter=imread('pic1.png');
  figure
  imshow(xcenter)
  ycenter=imnoise(xcenter,'salt & pepper',0.1);
  figure
  imshow(ycenter);
  c=imnoise(xcenter,'gaussian',0,0.1);
  figure
  imshow(c);
  d=imnoise(xcenter,'poisson');
  figure
  imshow(d);
  e=imnoise(xcenter,'speckle',0.2);
  figure
  imshow(e);
  %removing noise
  %% MEDIAN FILTER
  % noise salt and pepper
  clear
  clc
  xcenter=imread('pic1.png');
  ycenter=imnoise(xcenter,'salt & pepper',0.1);
  figure
  imshow(ycenter);
  aa=median_filter(ycenter,3);
  figure
  imshow(aa);
  %% noise gaussian
  xcenter=imread('pic1.png');
  ycenter=imnoise(xcenter,'gaussian',0,0.1);
  figure
  imshow(ycenter);
  aa=median_filter(ycenter,3);
  figure
  imshow(aa);
  %% noise poisson
  xcenter=imread('pic1.png');
  ycenter=imnoise(xcenter,'poisson');
  figure
  imshow(ycenter);
  aa=median_filter(ycenter,3);
  figure
  imshow(aa);
  %% noise speckle
  xcenter=imread('pic1.png');
  ycenter=imnoise(xcenter,'speckle',0.2);
  figure
  imshow(ycenter);
  aa=median_filter(ycenter,3);
  figure
  imshow(aa);
  %% gaussian filter
  %gaussian noise
clear
clc
x=imread('pic1.png');
[~, ~]=size(x);
xprime=imnoise(x,'gaussian',0,0.1);
figure
imshow(xprime);
f=conv2(xprime,gaussian2d(3,1),'same');
f=uint8(f);
figure
imshow(f)
%% salt and pepper
clear
clc
x=imread('pic1.png');
[~, ~]=size(x);
xprime=imnoise(x,'salt & pepper',0.1);
figure
imshow(xprime);
f=conv2(xprime,gaussian2d(3,1),'same');
f=uint8(f);
figure
imshow(f)
%% noise poisson
clear
clc
x=imread('pic1.png');
[~, ~]=size(x);
xprime=imnoise(x,'poisson');
figure
imshow(xprime);
f=conv2(xprime,gaussian2d(3,1),'same');
f=uint8(f);
figure
imshow(f)
%% noise speckle
clear
clc
x=imread('pic1.png');
[rw, col]=size(x);
xprime=imnoise(x,'speckle',0.2);
figure
imshow(xprime);
f=conv2(xprime,gaussian2d(25,1),'same');
f=uint8(f);
figure
imshow(f)

%% QUESTION 5
clear
clc
load fmri.mat
initialCorrelation=corr2(image(:,:,1),image(:,:,2));
%be vasileye imrotate va circshift tasvire 2ra dorost mikonim.
for teta=1:360
    x=imrotate(image(:,:,2),teta,'crop');
    for i=1:size(image,1)
        for j=1:size(image,2)
            xprime=circshift(x,[i j]);
            newCorr=corr2(xprime,image(:,:,1));
            if newCorr > initialCorrelation
                initialCorrelation=newCorr;
                teta2=teta;
                row=i;
                col=j;
            end
        end
    end
end
    image2=imrotate(image(:,:,2),teta2);
    imagefinal=circshift(image2,[row col]);
    figure
    imshow(image(:,:,1));
    figure
    imshow(imagefinal);
 %%
 

  
  
  
 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

  
  %% PART 1
  clear
  clc
%   function y=Gaussian_filter(x,kernelsize,sig)
%     xprime=zeros(2*kernelsize+1);
%     for k=1:size(x,1)
%         for l=1:size(x,2)
%             xprime(k+(kernelsize-1)/2,l+(kernelsize-1)/2)=x(k,l);
%         end
%     end
%     G=zeros(kernelsize);
%     for i=1:kernelsize
%         for j=1:kernelsize
%             G(i,j)=exp((-1)*(i^2+j^2)/(2*sig^2)/(2*pi*sig^2));    
%         end
%     end
%     y=zeros(size(x));
%     for i=1:size(x,1)
%         for j=1:size(x,2)
%             y(i,j)=sum(sum(x(i:i+kernelsize-1,j:j+kernelsize-1).*G(i,j)));
%         end
%     end
%     y=uint8(y);
% %y1=conv2(G,x);
% y=uint8(y1);
%     
%   end
  
  
  function y=median_filter(a,kernelsize)
    b=zeros(size(a));
    aprime=zeros(size(a)+kernelsize-1);
    for x=1:size(a,1)
        for y=1:size(a,2)
            aprime(x+(kernelsize-1)/2,y+(kernelsize-1)/2)=a(x,y);
        end
    end
    for i=1:size(aprime,1)-(kernelsize-1)
        for j=1:size(aprime,2)-(kernelsize-1)
            tensor=zeros(kernelsize*kernelsize,1);
            index=1;
            for x=1:kernelsize
                for y=1:kernelsize
                    tensor(index)=aprime(i+x-1,j+y-1);
                    index=index+1;
                end
            end
            m=sort(tensor);
            b(i,j)=m(ceil(kernelsize*kernelsize/2));
        end
    end
    y=uint8(b);
            
  
  end

 



function f=gaussian2d(N,sigma)
  % N is kernel size
 [x, y]=meshgrid(round(-N/2):round(N/2), round(-N/2):round(N/2));
 f=exp(-x.^2/(2*sigma^2)-y.^2/(2*sigma^2));
 f=f./sum(f(:));
end
            
