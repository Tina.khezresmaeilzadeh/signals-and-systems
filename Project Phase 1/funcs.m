%% Fun
function OutputSignal=HalfBandFFT(InputSignal,Fs)
    k=fft(InputSignal);
    for i=1:floor(length(InputSignal)/2)
        OutputSignal(i)=k(i);
    end

end
function out=bandpassfilter(y,Fw,Fs)
l=length(y);
new=zeros(1,l);
n1=floor(Fw(1,1)*2*l/Fs);
n2=floor(Fw(1,2)*2*l/Fs);
for u=1:l
    if u>n1
        if u<n2
    new(u)=y(u);
        end
    else
        new(u)=0;
    end
out=new;
end
end

function e=epoching(in,back,forward,stimuli)
    N=length(stimuli);
    T1=in(1,2)-in(1,1);
    Nback=floor(back/T1);
    Nforward=floor(forward/T1);
    samplenum=Nback+Nforward+1;
    A=zeros(8,samplenum,N);
    for sc=1:N
        A(1:8,1:samplenum,sc)=in(2:9,stimuli(1,sc)-Nback:stimuli(1,sc)+Nforward);
    end
    e=A;
end