%% y.mat
clear
clc
load y.mat
k=fft(y);
l=length(k);
figure;
t=-12663:12663;
subplot(2,2,1);
plot(t,abs(k));
title('abs with noise');
subplot(2,2,2);
plot(t,angle(k));
title('angle with noise');
r=real(y);
k2=fft(r);
k3=abs(k2);
k4=angle(k2);
subplot(2,2,3);
plot(t,k3);
title('abs without noise');
subplot(2,2,4);
title('angle without noise');
plot(t,k4);
error=fft(r-y);
figure;
subplot(2,1,1);
plot(t,abs(error));
title('abs error');
subplot(2,1,2);
plot(t,angle(error));
title('angle error');

%% sampling
clear
clc
load Subject1
T=SubjectData(1,2)-SubjectData(1,1);
ls=length(SubjectData(1,:));
freq=1/T;
    y=abs(HalfBandFFT(SubjectData(2,:),freq));
    filtered=bandpassfilter(y,[0.5 39],freq);
    result1=zeros(1,ls);
    t=freq*(1:ls/2)/ls;
    plot(t,y);
result2=zeros(1,ls);
for i=1:ls/2
    result1(i)=filtered(i);
    result1(ls+1-i)=result1(i);
    result2(i)=y(i);
    result2(ls+1-i)=result2(i); 
end
figure;
subplot(2,1,1);
t1 = freq*(1:ls)/ls;
plot(t1,abs(result1));
subplot(2,1,2);
plot(t1,abs(result2));  
pars1=0;
pars2=0;
xt=SubjectData(2,:);
pars1=dot(xt,xt);
f=fft(xt);
y=conj(f);
y=conj(y);
pars2=dot(f,y);
pars2=pars2/ls;

 pars1
 pars2

 %% sampling the last one
clc
n=60000;
fftsignal=zeros(1,n);
for i=1:n
    if(i<n/4)
        fftsignal(i)=1-4*i/n;
    elseif(i>=n/4 && i<(3*n/4))
        fftsignal(i)=0;
    else
        fftsignal(i)=-3+4*i/n;
    end
end

figure
k=2*pi*(1:n)/n;
plot(k,fftsignal);
signal=ifft(fftsignal);
figure;
k=2:n+1;
plot(k,abs(signal));
newsignal=zeros(1,n/3);
for i=2:3:n-1
        r=((i-2)/3)+1;
        newsignal(r)=signal(i);
    
end

figure;
k=2*pi*(1:n/3)/n;
plot(k,abs(fft(newsignal)));
%% eeg
clc
load Subject1
T=SubjectData(1,2)-SubjectData(1,1);
ls=length(SubjectData(1,:));
freq=1/T;
ftest=freq;


for j=2:9
    y=HalfBandFFT(SubjectData(j,:),freq);
    y=y-mean(y);
    fil=filter(Num,1,SubjectData(2,:));
    filtered=fft(fil);
result1=zeros(1,ls);
result2=zeros(1,ls);
for i=1:ls/2
    result1(i)=filtered(i);
    result1(ls+1-i)=result1(i);
    result2(i)=y(i);
    result2(ls+1-i)=result2(i); 
end
figure;
subplot(3,2,3);
t1 = ftest*(1:ls)/ls;
plot(t1,abs(result1));
title('not filtered');
subplot(3,2,5);
plot(t1,abs(result2));
title('filtered');
subplot(3,2,2);
plot(t1,abs(result2));
hold on;
plot(t1,abs(result1));

y2=ifft(y);
y3=ifft(result1);
N1=length(y2);
N2=length(y3);
n1 =ftest*(1:N1)/N1;
n2=ftest*(1:N2)/N2;
subplot(3,2,4);
plot(n2,real(y3));
title('filtered');
subplot(3,2,6);
plot(n1,real(y2));
title('not filtered');
end


%% EEG start
load Subject1.mat;
ls=length(SubjectData(1,:));
result1=zeros(9,ls);
result1(1,:)=SubjectData(1,:);
T=result1(1,2)-result1(1,1);
ftest=1/T;
for j=2:9
    y=HalfBandFFT(SubjectData(j,:),freq);
    y=y-mean(y);
    fil=filter(Num,1,SubjectData(2,:));
    filtered=fft(fil);
result=zeros(1,ls);
for i=1:ls/2
    result(i)=filtered(i);
    result(ls+1-i)=result(i);
end
result1(j,:)=real(ifft(result));
end
n=1; %%number of stimuli
for r=1:ls
    if SubjectData(10,r)~=0 
        if SubjectData(10,r-1)==0 
            stimuli(1,n)=r;
            n=n+1;
        end
    end
end
epochdata1=epoching(result1,0.2,0.8,stimuli);

energy1=Energy(epochdata1,1,30,256);

%% Fun
function OutputSignal=HalfBandFFT(InputSignal,Fs)
    k=fft(InputSignal);
    for i=1:floor(length(InputSignal)/2)
        OutputSignal(i)=k(i);
    end

end
function out=bandpassfilter(y,Fw,Fs)
l=length(y);
new=zeros(1,l);
n1=floor(Fw(1,1)*2*l/Fs);
n2=floor(Fw(1,2)*2*l/Fs);
for u=1:l
    if u>n1
        if u<n2
    new(u)=y(u);
        end
    else
        new(u)=0;
    end
out=new;
end
end

function e=epoching(in,back,forward,stimuli)
    N=length(stimuli);
    T1=in(1,2)-in(1,1);
    Nback=floor(back/T1);
    Nforward=floor(forward/T1);
    samplenum=Nback+Nforward+1;
    A=zeros(8,samplenum,N);
    for sc=1:N
        A(1:8,1:samplenum,sc)=in(2:9,stimuli(1,sc)-Nback:stimuli(1,sc)+Nforward);
    end
    e=A;
end
function energy=Energy(x, passband1,passband2,Fs)
N=length(x(1,1,:));
l=length(x(1,:,1));
e=zeros(8,N); 
n1=floor(l*passband1/Fs);
n2=floor(l*passband2/Fs);
en=0;
for r=1:N
    
    for j=1:8
        en=0;
      for i=n1:n2
          en=en+x(j,i,r)*x(j,i,r);
      end
      e(j,r)=en;
    end
    
end
energy=e;
end



