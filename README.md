﻿# Signals and Systems 

This course was taught by Dr. Aghajan.

**HW1:** Simulation of Voters Problem, Analyzing Z-Transform Properties

**HW2:** Analyzing Laplace-Transform and LTI Systems' Properties

**HW3:** Image Processing and Denoising, Implementation of Edge Detection Algorithms and Hough Transform

**Project Phase1:** Introduction to EEG Signals, Data Preprocessing and Implementing Learning Algorithm to Classify Specific Actions

**Project Phase 2:**
-  Predicting the word someone is thinking about using EEG Signals and SVM Method
