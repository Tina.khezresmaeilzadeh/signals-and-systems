fvhd a%% Tina KhezrEsmaeilzadeh
%% 96101595
%% Dr Karbalayi
%% QUESTION 1
clear
clc
t=linspace(-3,3,100);
y1=sin(t);
y2=cos(t).*heaviside(t);
y3=sin(t)+2*cos(2*t);

subplot(4,2,[1;3]);
plot(t,y1,'b');
axis([-3 4 -3 3]);
hold on
grid on
grid minor
plot(t,y2,'r--');
hold on
plot(t,y3,'go');
title('$for$ $i$ $\in$ i \{ 1,$2$,$3$,$4$ $\}$ $on$ $the$ $same$ $figure$','interpreter','latex');
legend('y1(t)','y2(t)','y3(t)');
xlabel('$t(s)$','Interpreter','latex')
ylabel('$y(V)$','Interpreter','latex')
%axis([0 10 -1 1]);
t1=linspace(0,10,500);
y1=sin(t1);
subplot(4,2,5);
plot(t1,y1);
text(pi,0,'\leftarrow sin(\pi)=0')

%axis([-10 20 0 1]);
t2=-10:1:20;
h=(1/2).^(t2).*heaviside(t2);
subplot(4,2,2);
stem(t2,h,'linewidth',1.5);
oldparam=sympref('HeavisideAtorigin',1);
title('$h[n]=(1/2)^nu[n]$','interpreter','latex');
grid on
grid minor
t3=0:2*pi/100:2*pi;
x4=2*cos(t3)+4;
y4=2*sin(t3)+3;
subplot(4,2,[4,6]);
plot(x4,y4);
grid on
hold on
x5=cos(t3)+3;
y5=sin(t3)+3;
plot(x5,y5);
hold on
x6=cos(t3)+5;
y6=sin(t3)+3;
plot(x6,y6);
hold on
x7=cos(t3)+4;
y7=sin(t3)+4;
plot(x7,y7,'g');
hold on
x8=cos(t3)+4;
y8=sin(t3)+2;
plot(x8,y8);
axis equal
syms z n;
eqn=z^2-1.8*(cos(pi/16))*z+0.81==0;
p = vpa(solve(eqn,z));
a1(n) = (p(1)^n);
a2(n) = (p(2)^n);
subplot(5,2,9:10);
plot(real(a1(-50:1:50)),imag(a1(-50:1:50)),'*','color','r');
title('$a1[n]$ $and$ $a2[n]$','interpreter','latex');
hold on
plot(real(a2(-50:1:50)),imag(a2(-50:1:50)),'*','color','b');
xlim([-0.4 1]);
ylim([-0.5 0.5]);
%% 
clear 
clc
syms n a1(n) a2(n) z;
syms z;
eqn=z^2-1.8*(cos(pi/16))*z+0.81==0;
p = vpa(solve(eqn,z));
a1(n) = (p(1)^n);
a2(n) = (p(2)^n);
plot(real(a1(-50:1:50)),imag(a1(-50:1:50)),'*');
hold on
plot(real(a2(-50:1:50)),imag(a2(-50:1:50)),'*');
xlim([-0.4 1]);
ylim([-0.5 0.5]);
%% QUESTION 2
 n=50;
 
 
 x=randi([1,3],n,n);

 imagesc(x);
 y=randi([1,n*n],625,1);
 
 %disp(y);
 counter=0;
 for time=1:500
      y=randi([1,n*n],625,1);
      colormap([1,0,0;0,1,0;0,0,1]);
     
     
     for i=1:625
         if mod(y(i),50)==0
             sotun=50;
         else
             sotun=mod(y(i),50);
         end
         satr=floor(y(i)/50)+1;
         
         if(satr==1)
             if(sotun==1)
                 
                     
                     choose=randi([1,3],1,1);

                     switch(choose)
                         case 1
                             x(1,1) = x(1,2);
                         case 2
                             x(1,1) = x(2,2);
                         case 3
                             x(1,1) = x(2,1);
                     end
                

             elseif(sotun==50)
                
                 
                         choose=randi([1,3],1,1);

                     switch(choose)
                         case 1
                             x(1,50) = x(1,49);
                         case 2
                             x(1,50) = x(2,99);
                         case 3
                             x(1,50)=x(3,50);
                     end
                 

             else 
                    
                             choose=randi([1,5],1,1);

                         switch(choose)
                             case 1
                                 x(1,sotun)=x(1,sotun-1);
                             case 2
                                 x(1,sotun)=x(2,sotun-1);
                             case 3
                                 x(1,sotun)=x(2,sotun);
                             case 4
                                 x(1,sotun)=x(2,sotun+1);
                             case 5
                                 x(1,sotun)=x(1,sotun+1);
                         end
                    
             end

         elseif satr==50
             if(sotun==1)
                 
                     choose=randi([1,3],1,1);
                     switch(choose)
                         case 1
                             x(50,1)=x(49,1);
                         case 2
                             x(1,1)=x(49,2);
                         case 3
                             x(1,1)=x(50,2);
                     end
                 

             elseif(sotun==50)
                
                         choose=randi([1,3],1,1);

                     switch(choose)
                         case 1
                             x(50,50) = x(49,50);
                         case 2
                             x(50,50) = x(49,49);
                         case 3
                             x(50,50) = x(50,49);
                     end
                 

             else 
                     
                             choose=randi([1,5],1,1);
                         switch(choose)
                             case 1
                                 x(50,sotun)=x(1,sotun-1);
                             case 2
                                 x(50,sotun)=x(2,sotun-1);
                             case 3
                                 x(50,sotun)=x(2,sotun);
                             case 4
                                 x(50,sotun)=x(2,sotun+1);
                             case 5
                                 x(50,sotun)=x(1,sotun+1);
                         end
                     
             end 
         elseif sotun==1
             
                     choose=randi([1,5],1,1);
                     %floor(y(i)/50)+1
                         switch(choose)
                             case 1
                                 x(satr,1)=x(satr-1,1);
                             case 2
                                 x(satr,1)=x(satr-1,2);
                             case 3
                                 x(satr,1)=x(satr,2);
                             case 4
                                 x(satr,1)=x(satr+1,2);
                             case 5
                                 x(satr,1)=x(satr+1,1);
                         end
             
         elseif sotun==1 && satr~=1
             
                     choose=randi([1,5],1,1);
                         switch(choose)
                             case 1
                                 x(satr,1)=x(satr-1,1);
                             case 2
                                 x(satr,1)=x(satr-1,2);
                             case 3
                                 x(satr,1)=x(satr,2);
                             case 4
                                 x(satr,1)=x(satr+1,2);
                             case 5
                                 x(satr,1)=x(satr+1,1);
                         end
             
         elseif satr~=1 && sotun~=1 && satr~=50 && sotun~=50
             
                     choose=randi([1,8],1,1);
                     
                     switch(choose)
                         case 1
                             x(satr,sotun)=x(satr-1,sotun-1);
                         case 2
                             x(satr,sotun)=x(satr-1,sotun);
                         case 3
                             x(satr,sotun)=x(satr-1,sotun+1);
                         case 4
                             x(satr,sotun)=x(satr,sotun+1);
                         case 5
                             x(satr,sotun)=x(satr+1,sotun+1);
                         case 6
                             x(satr,sotun)=x(satr+1,sotun);
                         case 7
                             x(satr,sotun)=x(satr+1,sotun-1);
                         case 8
                             x(satr,sotun)=x(satr,sotun-1);
                     end
             
         end
     end
     imagesc(x);
     counter=counter+1;
     
     title(counter);
     
     pause(0.08);
     
     
 end
 %%
 
 
 

%% QUESTION 1.2
%% part 1
figure
zplane(1,[1,-0.8]);
figure
impz(1,[1,-0.8]);
figure
X2=zplane(1,[1,-0.2]);
figure
impz(1,[1,-0.2]);
%%  part 2
 figure
 x3=zplane(1,[1,-1.2]);
 figure
 impz(1,[1,-1.2]);
figure
X4=zplane(1,[1,-4.8]);
figure
impz(1,[1,-4.8]);
%% part 3
figure
X5=zplane(1,[1,-1]);
figure
impz(1,[1,-1]);
figure
X6=zplane(1,[1,-2,1]);
figure
impz(1,[1,-2,1]);
%% part 4
figure
X7=zplane(0.5,[1,-1,0.5]);
figure
impz(0.5,[1,-1,0.5]);
figure
X8=zplane(1,[1,-2,2]);
figure
impz(1,[1,-2,2]);
%% Question 2.2

syms n x(n) X(z);
x(n) = cos(n*pi/4);
figure
stem(x(0:10));
 X(z) = ztrans(x);
 XC = collect(X);
 [num,den] = numden(XC);
 XF = tf(sym2poly(num),sym2poly(den),1,'variable','z');
 pzmap(XF);
 syms z X20(z) U(z)
 syms n x(n) x1(n) X20(z);
 x(n) = cos(n*pi/4);
 x1(n)=n*x(n);
 X20(z)=ztrans(x1);
 U = symfun(X20,z);
 XC2 = collect(U(z^2));
 [num2,den2] = numden(XC2);
 XF2 = tf(sym2poly(num2),sym2poly(den2),1,'variable','z');
 figure
 pzmap(XF2);
 axis equal
%% QUESTION 3.2
H=zplane([1,0.4*sqrt(2)],[1,-0.8*sqrt(2),0.64]); 
b=[1,0.4*sqrt(2)];
a=[1,-0.8*sqrt(2),0.64];
[r,p,k] = residuez(b,a);
 syms z HE;
HE=0;
for i=1:length(r)
    HE=HE+r(i)/(1-p(i)*(1/z));
end
HE;
HK=symfun(simplify((iztrans(HE))),n);
 l=0:100;
 HL=HK(l);
 stem(l,HL)

%%
syms z H(z) H1(z);
H(z)=(1+0.4*sqrt(2)*(z^(-1)))/(1-0.8*sqrt(2)*(z^(-1))+0.64*(z^(-2)));
H1(z)=iztrans(H(z));
H1(z)
%% QUESTION 4.2
clc
clear 
syms z H(z) n l HE HK HL;
%y(n)-1.8*cos(pi/16)*y(n-1)+0.81*y(n-2)=x(n)+0.5*x(n-1);
H(z)=(1+0.5*z^(-1))/(1-1.8*cos(pi/16)*z^(-1)+0.81*z^(-2));
b=[1,0.5];
a=[1,-1.8*cos(pi/16),0.81];
[r1,p1,k1] = residuez(b,a);
figure
impz([1,0.5],[1,-1.8*cos(pi/16),0.81]);
HE=0;
for i=1:length(r1)
    HE=HE+r1(i)/(1-p1(i)*(1/z));
end
format long
HE;
%% 4.1
%SADEH SHODE BALA
syms z H(z) n l HE HK HL;
 HR=(-0.5-3.94*i)/((0.88-0.1756i)/z-1)+(-0.5+3.94*i)/((0.88+0.176*i)/z-1);
HK=symfun(simplify((iztrans(HR))),n);
 l=0:100;
 HL=HK(l);
 stem(l,HL)
%d=abs(iztrans(HR));
%stem(1:10,d);
%% 4.2
syms z H(z) n l HE HK HL;
 HR=(-0.499-3.93*i)/((0.88-0.1758i)/z-1)+(-0.501+3.93*i)/((0.88+0.17558*i)/z-1);
HK=symfun(simplify((iztrans(HR))),n);
 l=0:100;
 HL=HK(l);
 stem(l,HL)

%% 4.3
b=[1,0.5];
a=[1,-1.8*cos(pi/16),0.81];
imp=[1;zeros(100,1)];
h=filter(b,a,imp);
stem(0:100,h)



































 
