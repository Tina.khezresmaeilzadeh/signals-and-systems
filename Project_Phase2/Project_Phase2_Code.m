%% filter design
clc
load('SubjectData1.mat');
subplot(1,2,1);
tina=train(3,:);
p=floor(length(train(3,:))/16);
h=impz(filter_1);
plot(tina(1:p),'r');
hold on
q=zphasefilter(h,train(3,:));
w=floor(length(zphasefilter(h,train(3,:)))/12);
plot(q(1:w),'b');
grid on
subplot(1,2,2);
o=floor(length(train(3,:))/12);
l=train(3,:);
plot(l(1:o),'r');
h=impz(filter_1);
hold on
o2=floor(length(conv(train(3,:),h))/12);
l2=conv(train(3,:),h);
plot(l2(1:o2),'b');
grid on
%% comparison with grpdelay  
gd = groupdelay(h',10000);
figure
plot(gd);
gd2=grpdelay(h',10000);
figure
plot(gd2);
%% which algorythm is used?

clc
load('SubjectData1.mat');
[r1, c1]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c1
    if train(11,i)==1
        finder1(1,n)=i;
        finder1(2,n)=train(10,i);
        n=n+1;
        
    end
end
%%%%subject data 1:single character

load('SubjectData2.mat');
[r2 c2]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c2
    if train(11,i)==1
        finder2(1,n)=i;
        finder2(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 2:single character

load('SubjectData3.mat');
[r3, c3]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c3
    if train(11,i)==1
        finder3(1,n)=i;
        finder3(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 3:row_column


load('SubjectData4.mat');
[r4, c4]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c4
    if train(11,i)==1
        finder4(1,n)=i;
        finder4(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 4:row_coloumn



load('SubjectData5.mat');
[r5, c5]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c5
    if train(11,i)==1
        finder5(1,n)=i;
        finder5(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 5:row_column


load('SubjectData6.mat');
[r6, c6]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c6
    if train(11,i)==1
        finder6(1,n)=i;
        finder6(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 6:row_column


load('SubjectData7.mat');
[r7, c7]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c7
    if train(11,i)==1
        finder7(1,n)=i;
        finder7(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 7:row_column


load('SubjectData8.mat');
[r8, c8]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c8
    if train(11,i)==1
        finder8(1,n)=i;
        finder8(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 8:row_column




load('SubjectData9.mat');
[r9, c9]=size(train);     %%%finding_size_of_train matrix
n=1;
for i=1:c9
    if train(11,i)==1
        finder9(1,n)=i;
        finder9(2,n)=train(10,i);
        n=n+1;
    end
end
%%%%subject data 9:row_column


%% 
clear
clc
load('SubjectData1.mat');
sub(1).train = train;
sub(1).test = test;
load('SubjectData2.mat');
sub(2).train = train;
sub(2).test = test;
load('SubjectData3.mat');
sub(3).train = train;
sub(3).test = test;
load('SubjectData4.mat');
sub(4).train = train;
sub(4).test = test;
load('SubjectData5.mat');
sub(5).train = train;
sub(5).test = test;
load('SubjectData6.mat');
sub(6).train = train;
sub(6).test = test; 
load('SubjectData7.mat');
sub(7).train = train;
sub(7).test = test;
load('SubjectData8.mat');
sub(8).train = train;
sub(8).test = test;
load('SubjectData9.mat');
sub(9).train = train;
sub(9).test = test;
sub=indexextraction(sub);


%epoching
h = BPF(10,1,40,1/0.0039);
for i=1:9
    for j=2:9
        sub(i).train(j,:)=filtfilt(h,1,sub(i).train(j,:));
    end
end
[z1,z2]=epochingsvm(sub(1).train,50,200);
X=[];
me=[];
v=[];
mo=[];
mef=[];
for i=1:8 %satr
    for k=1:size(z1,3) %sootoon
        
        signal=z1(i,:,k);
        Mean(k,1)=mean(signal);  %1.miangin
        Var(k,1)=var(signal,0,'all');   %2.variance
        Mode(k,1)=mode(signal);      %3.mode
        Meanf(k,1)=meanfreq(signal);    %4.frekanse miangin
        
    end
    
    
    me=[me Mean];
    v=[v Var];
    mo=[mo Mode];
    mef=[mef Meanf];
    
end
%5.signale zamani
signal=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    S=[];
    for k=1:size(z1,3) %sootoon
        signal=z1(i,:,k);
        S=[S;signal];
    end      
    XSignal=[XSignal S];
end
X=[me v  mo XSignal mef];





[z1,z2]=epoching_test(sub(1).train,50,200); %YOU CAN PUT ANY OTHER DATASET

X1=[];
me1=zeros(size(z1,3),8);
v1=zeros(size(z1,3),8);
mo1=zeros(size(z1,3),8);
mef1=zeros(size(z1,3),8);
Mean=zeros(size(z1,3),1);
Var=zeros(size(z1,3),1);
Mode=zeros(size(z1,3),1);
Meanf=zeros(size(z1,3),1);
for i=1:8 %satr
    for k=1:size(z1,3) %sootoon
        
        signal1=z1(i,:,k);
        Mean(k,1)=mean(signal1);
        Var(k,1)=var(signal1,0,'all');
        Mode(k,1)=mode(signal1);
        Meanf(k,1)=meanfreq(signal1);
        
    end
    
    
    me1=[me1 Mean];
    v1=[v1 Var];
    mo1=[mo1 Mode];
    mef1=[mef1 Meanf];
    
end

signal1=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    S=[];
    for k=1:size(z1,3) %sootoon
        signal1=z1(i,:,k);
        S=[S;signal1];
    end      
    XSignal=[XSignal S];
end
X1=[me1 v1  mo1 XSignal mef1];


X2=[];
me2=zeros(size(z2,3),8);
v2=zeros(size(z2,3),8);
mo2=zeros(size(z2,3),8);
mef2=zeros(size(z2,3),8);
Mean=zeros(size(z2,3),1);
Var=zeros(size(z2,3),1);
Mode=zeros(size(z2,3),1);
Meanf=zeros(size(z2,3),1);
for i=1:8 %satr
    for k=1:size(z2,3) %sootoon
        signal2=z2(i,:,k);
        Mean(k,1)=mean(signal2);
        Var(k,1)=var(signal2,0,'all');
        Mode(k,1)=mode(signal2);
        Meanf(k,1)=meanfreq(signal2);
        
    end
    me2=[me2 Mean];
    v2=[v2 Var];
    mo2=[mo2 Mode];
    mef2=[mef2 Meanf];
    
end
signal2=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    S=[];
    for k=1:size(z2,3) %sootoon
        signal1=z2(i,:,k);
        S=[S;signal2];
    end      
    XSignal=[XSignal S];
end


X2=[me2 v2 XSignal mo2 mef2];







%%
J = [];
for i = 1 : size(X,2)
   Mean0 = mean(X1(:,i));
   Mean1= mean(X2(:,i));
   MeanTotal = mean(X(:,i));
   Var0 = var(X1(:,i),0,'all');
   Var1 = var(X2(:,i),0,'all');
   newj = (abs(MeanTotal-Mean0)+abs(MeanTotal-Mean1))/(Var0+Var1);
   J = [J;newj];
end
Y=zeros(2700,1);
for i=1:size(z1,3)
    Y(i)=1;
end
%% cross validation
C=[];
th=[];
oo=[];
for h=0.1:0.1:0.6
    threshold=floor(h*size(X,1));
    XNEW=[]; 
    for i=1:size(J)
        if(J(i)>=J(threshold))
            XNEW=[XNEW X(:,i)];
            
        end
    end
    XCROSS=XNEW(1:floor(0.7*size(XNEW,1)),:);
    YCROSS=Y(1:floor(0.7*size(Y,1)));
    XCHECK=XNEW(floor(0.7*size(XNEW,1))+1:size(XNEW,1),:);
    YCHECK=Y(floor(0.7*size(Y,1))+1:size(Y,1));
    svm=fitcsvm(XCROSS,YCROSS);
    
    d= predict(svm,XCHECK,'classnames',[1 0],'cost',[0 35;1 0]);
    count=0;
    for i=1:size(YCHECK,1)
        if d(i)==YCHECK(i)
        count=count+1;
        end
    end
    C=[C count];
    th=[th threshold];  
end
for i=1:length(C)
    if max(C)==C(i)
        bestThreshold=th(i);
    end
end
%% SVM
[z1,z2]=epochingsvm(sub(1).test,50,200);%you have 9 choices for testing
%svm=fitcsvm(XCROSS,YCROSS);
X=[];
me1=zeros(size(z1,3),8);
v1=zeros(size(z1,3),8);
mo1=zeros(size(z1,3),8);
mef1=zeros(size(z1,3),8);
Mean=zeros(size(z1,3),1);
Var=zeros(size(z1,3),1);
Mode=zeros(size(z1,3),1);
Meanf=zeros(size(z1,3),1);
for i=1:8 %satr
    for k=1:size(z1,3) %sootoon
        signal1=z1(i,:,k);
        Mean(k,1)=mean(signal1);
        Var(k,1)=var(signal1,0,'all');
        Mode(k,1)=mode(signal1);
        Meanf(k,1)=meanfreq(signal1);
        
    end
    me1=[me1 Mean];
    v1=[v1 Var];
    mo1=[mo1 Mode];
    mef1=[mef1 Meanf];
    
end
signal1=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    
    S=[];
    for k=1:size(z1,3) %sootoon
        signal1=z1(i,:,k);
        S=[S;signal1];
    end      
    XSignal=[XSignal S];
end
X=[me1 v1 XSignal mo1 mef1];
J = [];
for i = 1 : size(X,2)
   Mean0 = mean(X1(:,i));
   Mean1= mean(X2(:,i));
   MeanTotal = mean(X(:,i));
   Var0 = var(X1(:,i),0,'all');
   Var1 = var(X2(:,i),0,'all');
   newj = (abs(MeanTotal-Mean0)+abs(MeanTotal-Mean1))/(Var0+Var1);
   J = [J;newj];
end
Y=zeros(2700,1);
for i=1:size(z1,3)
    Y(i)=1;
end

    XNEW=[]; 
    for i=1:size(J)
        if(J(i)>=J(bestThreshold))
            XNEW=[XNEW X(:,i)];
        end
    end
    XCROSS=XNEW(1:floor(0.7*size(XNEW,1)),:);
    
  
    d= predict(svm,XCHECK);
   
    

d= predict(svm,Xtest);

letter=[];
for i=1:size(Y,1)
    if(d(i)~=0)
        letter=[letter z2(i)];
    end
end
%% LDA

LDA = fitcdiscr(XCROSS,YCROSS,'classnames',[1 0],'cost',[0 35;1 0]);
[z1,z2]=epochingsvm(sub(1).test,50,200);%you have 9 choices for testing
%svm=fitcsvm(XCROSS,YCROSS);
X=[];
me1=zeros(size(z1,3),8);
v1=zeros(size(z1,3),8);
mo1=zeros(size(z1,3),8);
mef1=zeros(size(z1,3),8);
Mean=zeros(size(z1,3),1);
Var=zeros(size(z1,3),1);
Mode=zeros(size(z1,3),1);
Meanf=zeros(size(z1,3),1);
for i=1:8 %satr
    for k=1:size(z1,3) %sootoon
        signal1=z1(i,:,k);
        Mean(k,1)=mean(signal1);
        Var(k,1)=var(signal1,0,'all');
        Mode(k,1)=mode(signal1);
        Meanf(k,1)=meanfreq(signal1);
        
    end
    me1=[me1 Mean];
    v1=[v1 Var];
    mo1=[mo1 Mode];
    mef1=[mef1 Meanf];
    
end
signal1=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    
    S=[];
    for k=1:size(z1,3) %sootoon
        signal1=z1(i,:,k);
        S=[S;signal1];
    end      
    XSignal=[XSignal S];
end
X=[me1 v1 XSignal mo1 mef1];
J = [];
for i = 1 : size(X,2)
   Mean0 = mean(X1(:,i));
   Mean1= mean(X2(:,i));
   MeanTotal = mean(X(:,i));
   Var0 = var(X1(:,i),0,'all');
   Var1 = var(X2(:,i),0,'all');
   newj = (abs(MeanTotal-Mean0)+abs(MeanTotal-Mean1))/(Var0+Var1);
   J = [J;newj];
end
Y=zeros(2700,1);
for i=1:size(z1,3)
    Y(i)=1;
end

    XNEW=[]; 
    for i=1:size(J)
        if(J(i)>=J(bestThreshold))
            XNEW=[XNEW X(:,i)];
        end
    end
    XCROSS=XNEW(1:floor(0.7*size(XNEW,1)),:);
    
  
    d= predict(svm,XCHECK);
   
    
fprintf('hello');
d= predict(svm,Xtest);
fprintf('bye');
letter=[];
for i=1:size(Y,1)
    if(d(i)~=0)
        letter=[letter z2(i)];
    end
end




%%
function s=indexextraction(sub)
    for j=1:9
    [~,c]=size(sub(j).train);     %%%finding_size_of_train matrix
    [~,c2]=size(sub(j).test);
    n1=1;
    n2=1;
    n3=1;
    for i=1:c
        if (sub(j).train(10,i)~= 0)&&(sub(j).train(10,i+3)~= 0)
           if sub(j).train(11,i)==1
               if n1==1
                  sub(j).timetrain.target(n1)=sub(j).train(1,i); 
                  n1=n1+1;   
               end
               if (sub(j).train(10,i)-sub(j).timetrain.target(n1-1))>0.018
                  sub(j).timetrain.target(n1)=sub(j).train(1,i); 
                  n1=n1+1;
               end
           else
               if n2==1
                  sub(j).timetrain.nontarget(n2)=sub(j).train(1,i); 
                  n2=n2+1;   
               end
              if (sub(j).train(10,i)-sub(j).timetrain.nontarget(n2-1))>0.018
                     sub(j).timetrain.nontarget(n2)=sub(j).train(1,i);
                     n2=n2+1;
              end
           end
        end
    end

     for k=1:c2
        if (sub(j).test(10,k)~= 0)&&(sub(j).test(10,k+3)~= 0)

               if n3==1
                  sub(j).timetest(n3)=sub(j).test(1,k); 
                  n3=n3+1;   
               else
                  sub(j).timetest(n3)=sub(j).test(1,k); 
                  n3=n3+1;
               end
       end

    end
    end

    s=sub;
end 


function [z1,z2,z3,z4]=epoching_test(train,backwardsamples,forwardsamples)
x=train;
f=forwardsamples;
b=backwardsamples;
counter1=0;
counter2=0;
l=1;
m=1;
    for j=1:length(train(1, :))
        if((train(10,j)~=0)&&(train(10,j-1)==0))
            
           
            if(train(11,j)~=0)
                z3(l)=train(1,j);
                l=l+1;
                counter1=counter1+1;
                for i=2:9
                    u=1;
                    for k=j-b:j+f
                        z1(i-1,u,counter1)=x(i,k);
                        u=u+1;
                    end
                end
            end
            if train(11,j)==0
                z4(m)=train(1,j);
                m=m+1;
                    counter2=counter2+1;
                for i=2:9
                    u=1;
                    for k=j-b:j+f
                        z2(i-1,u,counter2)=x(i,k);
                        u=u+1;
                    end
                end
            end
                
        end
    end
end            
            


function [z1,z2]=epochingsvm(test,backwardsamples,forwardsamples)
x=test;
f=forwardsamples;
b=backwardsamples;
counter1=0;

    for j=1:length(test(1, :))
        if((test(10,j)~=0)&&(test(10,j-1)==0))
            counter1=counter1+1;
            z2(counter1)=test(10,j);
            for i=2:9
                u=1;
                for k=j-b:j+f
                    z1(i-1,u,counter1)=x(i,k);
                    u=u+1;
                    
                    
                end
            end
        end
        
    end
end            
            

function gd = groupdelay(h,N)
n = [0:length(h)-1];
h2 = n .* h;
gd = real(fft(h2,N) ./ fft(h,N)); 
 end


function y = zphasefilter(h,x)
N = 10000;
yy = filter(h,1,x);
gd = round(groupdelay(h,N));
gd = gd(~isnan(gd));
gd = gd(~isinf(gd));
gd = gd(abs(gd) < 10000);
gdnew = round(mean(gd));
y = yy((gdnew+1): end);
end






            
            
            
            
            
            
            