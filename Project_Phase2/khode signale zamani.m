clear
clc
load('SubjectData1.mat');
sub(1).train = train;
sub(1).test = test;
load('SubjectData2.mat');
sub(2).train = train;
sub(2).test = test;
load('SubjectData3.mat');
sub(3).train = train;
sub(3).test = test;
load('SubjectData4.mat');
sub(4).train = train;
sub(4).test = test;
load('SubjectData5.mat');
sub(5).train = train;
sub(5).test = test;
load('SubjectData6.mat');
sub(6).train = train;
sub(6).test = test;
load('SubjectData7.mat');
sub(7).train = train;
sub(7).test = test;
load('SubjectData8.mat');
sub(8).train = train;
sub(8).test = test;
load('SubjectData9.mat');
sub(9).train = train;
sub(9).test = test;



%epoching
h = BPF(10,1,40,1/0.0039);
for i=1:9
    for j=2:9
        sub(i).train(j,:)=filtfilt(h,1,sub(i).train(j,:));
    end
end
[z1,z2]=epochingsvm(sub(1).train,50,200);

X=[];
signal=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    S=[];
    for k=1:size(z1,3) %sootoon
        signal=z1(i,:,k);
        S=[S;signal];
    end      
    XSignal=[XSignal S];
end
X=[XSignal];
% YAFTANE X1 VA X2
[z1,z2]=epoching_test(sub(1).train,50,200); %YOU CAN PUT ANY OTHER DATASET

X1=[];





signal1=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    S=[];
    for k=1:size(z1,3) %sootoon
        signal1=z1(i,:,k);
        S=[S;signal1];
    end      
    XSignal=[XSignal S];
end
X1=[XSignal];

X2=[];

signal2=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    S=[];
    for k=1:size(z2,3) %sootoon
        signal1=z2(i,:,k);
        S=[S;signal2];
    end      
    XSignal=[XSignal S];
end


X2=[XSignal];
%%
J = [];
for i = 1 : size(X,2)
   Mean0 = mean(X1(:,i));
   Mean1= mean(X2(:,i));
   MeanTotal = mean(X(:,i));
   Var0 = var(X1(:,i),0,'all');
   Var1 = var(X2(:,i),0,'all');
   newj = (abs(MeanTotal-Mean0)+abs(MeanTotal-Mean1))/(Var0+Var1);
   J = [J;newj];
end
Y=zeros(2700,1);
for i=1:size(z1,3)
    Y(i)=1;
end
XC=[X Y];
%%
C=[];
th=[];
for h=0.1:0.1:0.6
    threshold=floor(h*size(X,1));
    XNEW=[]; 
    indexX=[];
    for i=1:size(J)
        if(J(i)>=J(threshold))
            XNEW=[XNEW X(:,i)];
            indexX=[indexX i];
        end
    end
    XCROSS=XNEW(1:floor(0.7*size(XNEW,1)),:);
    YCROSS=Y(1:floor(0.7*size(Y,1)));
    XCHECK=XNEW(floor(0.7*size(XNEW,1))+1:size(XNEW,1),:);
    YCHECK=Y(floor(0.7*size(Y,1))+1:size(Y,1));
    svm=fitcsvm(XCROSS,YCROSS,'classnames',[1 0],'cost',[0 35;1 0]);
    
    d= predict(svm,XCHECK);
    count=0;
    for i=1:size(YCHECK,1)
        if d(i)==YCHECK(i)
        count=count+1;
        end
    end
    C=[C count];
    th=[th threshold];  
end
for i=1:length(C)
    if max(C)==C(i)
        bestThreshold=th(i);
    end
end

%%

[z1,z2]=epochingsvm(sub(1).test,50,200);
X=[];
signal=zeros(1,251);
XSignal=[];
for i=1:8 %satr
    S=[];
    for k=1:size(z1,3) %sootoon
        signal=z1(i,:,k);
        S=[S;signal];
    end      
    XSignal=[XSignal S];
end
X=[XSignal];

%%
XPRIME=[];

for i=1:size(indexX,2)
  
    XPRIME=[XPRIME X(:,indexX(i))];
end

d= predict(svm,XPRIME);
%% finding letters

    

    

%% function

function [z1,z2]=epochingsvm(test,backwardsamples,forwardsamples)
x=test;
f=forwardsamples;
b=backwardsamples;
counter1=0;

    for j=1:length(test(1, :))
        if((test(10,j)~=0)&&(test(10,j-1)==0))
            counter1=counter1+1;
            z2(counter1)=test(10,j);
            for i=2:9
                u=1;
                for k=j-b:j+f
                    z1(i-1,u,counter1)=x(i,k);
                    u=u+1;
                    
                    
                end
            end
        end
        
    end
end            

function [z1,z2,z3,z4]=epoching_test(train,backwardsamples,forwardsamples)
x=train;
f=forwardsamples;
b=backwardsamples;
counter1=0;
counter2=0;
l=1;
m=1;
    for j=1:length(train(1, :))
        if((train(10,j)~=0)&&(train(10,j-1)==0))
            
           
            if(train(11,j)~=0)
                z3(l)=train(1,j);
                l=l+1;
                counter1=counter1+1;
                for i=2:9
                    u=1;
                    for k=j-b:j+f
                        z1(i-1,u,counter1)=x(i,k);
                        u=u+1;
                    end
                end
            end
            if train(11,j)==0
                z4(m)=train(1,j);
                m=m+1;
                    counter2=counter2+1;
                for i=2:9
                    u=1;
                    for k=j-b:j+f
                        z2(i-1,u,counter2)=x(i,k);
                        u=u+1;
                    end
                end
            end
                
        end
    end
end            
            

