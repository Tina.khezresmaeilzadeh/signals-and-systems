%Tina KhezrEsmaeilzadeh
%96101595
% Dr Karbalayi
%% Question 1
syms t s;
f1(t)=5*exp(-5*t)*heaviside(t);
lf1=laplace(f1(t))
f2(t)=5*t*exp(-5*t)*heaviside(t);
lf2=laplace(f2(t))
f3(t)=(t*sin(2*t)+exp(-2*t))*heaviside(t);
lf3=laplace(f3(t))
f4(t)=5*t*t*exp(-5*t)*heaviside(t);
lf4=laplace(f4(t))
%% Question 2
syms s t;
F1(s)=28/(s*(s+8));
lf1=ilaplace(F1(s));
F2(s)=(s-5)/(s*(s+2)^2);
lf2=ilaplace(F2(s));
F3(s)=10/((s+1)^2*(s+3));
lf3=ilaplace(F3(s))
F4(s)=2*(s+1)/(s^2+s+2);
lf4=ilaplace(F4(s));
%% Question 3
%a
syms s t;
G(s)=25/(s^2+4*s+25);
lf=ilaplace(G(s));
%impulse response
figure
fplot(lf)
%pulse response
 pulseresponse=ilaplace(G(s)/s);
 sys1=tf(25,[1,4,25]);
 figure
 fplot(pulseresponse);
 %b
 figure
 bode(sys1);

%% Question 2

figure
subplot(4,2,[1,3])
sys1=tf([0,0,0,1],[1,20,10,400]);
p1=pole(sys1);
pzmap(sys1);
title('pole zero map H1');
sgrid
subplot(4,2,[2,4]);
step(sys1,20);
title('step response H1');
subplot(4,2,[5,7]);
sys2=tf([0,0,0,0,1],[1,12.5,10,10,1]);
p2=pole(sys2);
pzmap(sys2);
title('pole zero map H2');
sgrid
subplot(4,2,[6,8]);
step(sys2,40);
title('step response H2');
figure
subplot(4,2,[1,3])
sys3=tf([0,0,0,0,0,0,1],[1,5,125,100,100,20,10]);
p3=pole(sys3);
pzmap(sys3);
title('pole zero map H3');
sgrid
subplot(4,2,[2,4]);
step(sys3,50);
title('step response H3');
subplot(4,2,[5,7]);
sys4=tf([0,0,0,0,1],[1,125,100,100,20,10]);
p4=pole(sys4);
pzmap(sys4);
title('pole zero map H4');
sgrid
subplot(4,2,[6,8]);
step(sys4,50);
title('step response H4');






%% Question 4
syms s t;
G1(s)=(s+1)/(s^2+5*s+6);
x1(t)=dirac(t);
x2(t)=heaviside(t);
x3(t)=sin(2*t)*heaviside(t);
x4(t)=exp(-t)*heaviside(t);
h1=ilaplace(G1(s)*laplace(x1(t)));
h2=ilaplace(G1(s)*laplace(x2(t)));
h3=ilaplace(G1(s)*laplace(x3(t)));
h4=ilaplace(G1(s)*laplace(x4(t)));
figure
fplot(h1);
figure
fplot(h2);
figure
fplot(h3);
figure
fplot(h4);
%% Question 5
syms s t; 
G2(s)=(10*s+4)/(s^2+4*s+4);
lf(t)=ilaplace(G2(s)/s);
fplot(lf(t))
x1=limit(lf,t,inf);
%the time that the response is maximum
eqn=diff(lf(t))==0;
p = vpa(solve(eqn,t));
%max value of response
lf(p)
eqn2=lf(t)==lf(p)/2;
p2 = vpa(solve(eqn2,t));
%% Question 3
syms s t;
H(s)=1/(s^2+s-2);
poles1=tf([0,0,1],[1,1,-2]);
poles2=tf(1,1);
figure
pzmap(poles1);
Y=feedback(poles1,poles2);
figure
pzmap(Y);
%% Question 3.3
clear 
clc
syms s t;
for k=-10:2:10
    poles1=tf([0,0,k],[1,1,-2]);
    poles2=tf(1,1);
    Y=feedback(poles1,poles2);
    fprintf("k=%d\n",k)
    p=pole(Y)
    figure
    subplot(1,2,1)
    pzmap(Y);
    subplot(1,2,2)
    rlocus(Y);
    
end 
%% Question 4







